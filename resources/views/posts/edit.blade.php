@extends('layouts.app')

@section('content')
<form method="PUT" action="/posts/{{$post->id}}">
    <div class="form-group my-3">
      <label for="titleInput">Title:</label>
      <input type="text" class="form-control" id="titleInput" value={{$post->title}}>
    </div>
    <div class="form-group my-3">
      <label for="contentInput">Content:</label>
      <textarea type="text" class="form-control" id="contentInput" value={{$post->content}} >{{$post->content}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Update Post</button>
  </form>
@endsection