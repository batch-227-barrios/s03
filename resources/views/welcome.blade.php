@extends('layouts.app')

@section('content')
<div class="d-flex justify-content-center align-items-center flex-column">
    <img src="https://laravelnews.imgix.net/images/laravel-featured.png" class="img-fluid w-50" alt="laravelIMG">
<h2>Featured Posts:</h2>
    @if(count($posts) > 0)
        {{-- @foreach(array_slice($posts, 0, 3 ) as $post) --}}
        @foreach($posts as $post)
            <div class="card text-center w-100 m-3">
                <div class="card-body">
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                </div>

            </div>
        @endforeach
    @else
        <div>
            <h2>There are no posts to show</h2>
            <a href="/posts/create" class="btn btn-info">Create post</a>
        </div>
    @endif
</div>
@endsection