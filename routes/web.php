<?php

use Illuminate\Support\Facades\Route;
// link the PostControlle file
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/about', function () {
    return view('about');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//define a route wherein a view to create a post that will be returned to the user (form for creating a post).
Route::get('/posts/create', [PostController::class, 'create']);

//define a route wherein form data will be sent via POST method to the /posts URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

//define a route that will return a view containing all the posts.
Route::get('/posts', [PostController::class, 'index']);

// define a route that will return a view for the welcome page.
Route::get('/', [PostController::class, 'welcome']);

// define a route that will return a view containing only the authenticated user's posts
Route::get('/myPosts', [PostController::class, 'myPosts']);

// define a route wherein a view showing a specific post with matching URL parameter $id to query for the database entry to be shown.
Route::get('/posts/{id}', [PostController::class, 'show']);

Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

Route::put('/posts/{id}', [PostController::class, 'storeEdit']);
